using namespace std;
#include <iostream>
#include <map>
#include <vector>
typedef unsigned int DWORD;

struct ContactPair {
  ContactPair (DWORD one = 0, DWORD two = 0) : m_one(one), m_two(two) {}
  DWORD m_one, m_two;
};

class Test;

class NeutralRelay {
  friend class Test;
  
  public :
    enum R_STATE {
      R_STATE_OFF = 0,
      R_STATE_ON  = 1
    };
    typedef vector<ContactPair>               ONE_STATE_CONTACTS;
    typedef map<R_STATE, ONE_STATE_CONTACTS>  CONTACTS_GROUPS;

    NeutralRelay (int (*c_groups)[ 3 ],int group_count) : m_state (R_STATE_OFF) {
      m_groupNumber = group_count;
      ONE_STATE_CONTACTS on_contacts;
      ONE_STATE_CONTACTS off_contacts;
      for (DWORD i = 0; i < m_groupNumber; ++i) {
        off_contacts.push_back( ContactPair(c_groups[i][0], c_groups[i][1]) );
        on_contacts.push_back( ContactPair(c_groups[i][1], c_groups[i][2]) );
      }
      m_contacts[R_STATE_ON] = on_contacts;
      m_contacts[R_STATE_OFF] = off_contacts;
    }
    
    const char* state() const { return R_STATE_ON == m_state ? "ON" : "OFF"; }
    
    void power_on() { m_state = R_STATE_ON; }
    
    void power_off() { m_state = R_STATE_OFF; }
    
    ONE_STATE_CONTACTS connected_contacts(){ return m_contacts[m_state]; }
    
    bool are_connected(DWORD ctn1, DWORD ctn2) { return contact_connected_to(ctn1) == ctn2; }
    
    DWORD contact_connected_to(DWORD contact) {
      ONE_STATE_CONTACTS conts = connected_contacts();
      for (DWORD i = 0; i < m_groupNumber; ++i) {
        if(contact == conts[i].m_one) return conts[i].m_two;    
        if(contact == conts[i].m_two) return conts[i].m_one;            
      }
      return 0;
    }
    
  private:
    R_STATE           m_state; 
    DWORD             m_groupNumber;
    CONTACTS_GROUPS   m_contacts;
};

class Test {
  public:
  
  Test(NeutralRelay& relay) {
    m_pRelay = &relay;
  }
  
  void test_contact(DWORD contact) { cout << contact << " is connected to " << m_pRelay->contact_connected_to(contact) << endl; }

  void test_contact_pair(DWORD ctn1, DWORD ctn2) { 
    cout << ctn1 << " and " << ctn2 << " are connected: " << m_pRelay->are_connected(ctn1, ctn2) << endl;    
  }

  void print_connected_contacts() {
    NeutralRelay::ONE_STATE_CONTACTS conts = m_pRelay->connected_contacts();
    cout << "Connected contacts: [";
    for (DWORD i = 0; i < m_pRelay->m_groupNumber; ++i) {
        cout << '[' << conts[i].m_one << ',' << conts[i].m_two << ']';
        if(i < m_pRelay->m_groupNumber - 1){ cout << ','; }
    }
    cout << "]" << endl;
  }
  
  void do_test() {
    cout << "State: " << m_pRelay->state() << endl;
    print_connected_contacts();
    test_contact(1);
    test_contact(9);
    test_contact(5);
    test_contact_pair(1, 9);
    test_contact_pair(9, 1);
    test_contact_pair(9, 5);
  }
  
  private:
    NeutralRelay* m_pRelay;
};

int main() {
  int contact_groups[][3] = { {1, 9, 5}, {2, 10, 6}, {3, 11, 7}, {4, 12, 8} };
  int group_count = sizeof(contact_groups)/sizeof(int[3]);
  NeutralRelay relay(contact_groups,group_count);
  Test tester(relay);
  tester.do_test();
  relay.power_on();
  tester.do_test();
}
