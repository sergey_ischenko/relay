using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
 
class NeutralRelay {
 
  private IDictionary<bool, IList<int[]>> contacts = new Dictionary<bool, IList<int[]>>();
  private bool innerState = false;
 
  public NeutralRelay(int[][] contactGroups){
    contacts[false] = contactGroups.Select(group => {return group.Take(2).ToArray();}).ToList();
    contacts[true] = contactGroups.Select(group => {return group.Skip(1).Take(2).ToArray();}).ToList();
  }
 
  public string state(){ return innerState ? "ON" : "OFF"; }
 
  public void PowerOn(){ innerState = true; }
 
  public void PowerOff(){ innerState = false; }
 
  public IList<int[]> ConnectedContacts(){ return contacts[innerState]; }
 
  public int ContactConnectedTo(int contact){
    int[] contactPair = ConnectedContacts().FirstOrDefault(pair => pair.Any(c => c == contact));
    return (null == contactPair) ? 0 : contactPair.FirstOrDefault(cont => cont != contact);
  }
 
  public bool AreConnected(int contact1, int contact2){
    return ContactConnectedTo(contact1) == contact2;
  }
  
}
 
/////////////////// Example of using (relay HH54P)
 
class Test {
  // There is no standard method to print array...
  private static string ListToString(IEnumerable<string> stringList){
    return "[" + String.Join(", ", stringList.ToArray()) + "]";
  }
  private static void PrintConnectedContacts(NeutralRelay relay){
    IEnumerable<String> pairStrings = relay.ConnectedContacts().Select( pair => { return ListToString(pair.Select(c => c.ToString())); } );
    Console.WriteLine( "Connected contacts: " + ListToString(pairStrings) );
  }
  
  private static void TestContact(NeutralRelay relay, int contact){
    Console.WriteLine(contact + " is connected to " + relay.ContactConnectedTo(contact));
  }
 
  private static void TestContactPair(NeutralRelay relay, int contact1, int contact2){
    Console.WriteLine(contact1 + " and " + contact2 + " are connected: " +  relay.AreConnected(contact1, contact2));
  }
 
  private static void DoTest(NeutralRelay relay){
    Console.WriteLine("State: " + relay.state());
    PrintConnectedContacts(relay);
    TestContact(relay, 1); 
    TestContact(relay, 9); 
    TestContact(relay, 5); 
    TestContactPair(relay, 1,9); 
    TestContactPair(relay, 9,1); 
    TestContactPair(relay, 9,5);
  }
 
  static void Main(string[] args){
    int[][] contactGroups = { 
      new int[] {1, 9, 5}, 
      new int[] {2, 10, 6}, 
      new int[] {3, 11, 7}, 
      new int[] {4, 12, 8} 
    };
    NeutralRelay relay = new NeutralRelay(contactGroups);
    DoTest(relay);    
    relay.PowerOn();   
    DoTest(relay);
  }
}