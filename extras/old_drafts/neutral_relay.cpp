#include <iostream>
using namespace std;

class NeutralRelay {

  private:
  
    int** pairs_on;
    int** pairs_off;
    int inner_state;
    int group_count;
  
  public: 
   
    NeutralRelay(int (*contact_groups)[3]){
      group_count = sizeof(contact_groups) / 3*sizeof(int);

      pairs_on = new int*[group_count];
      pairs_off = new int*[group_count];
    
      for (int g = 0; g < group_count; g++) {
        int pair_off[2];
        pairs_off[g] = pair_off;
        pair_off[0] = contact_groups[g][0];
        pair_off[1] = contact_groups[g][1];

        int pair_on[2];
        pairs_on[g] = pair_on;
        pair_on[0] = contact_groups[g][1];
        pair_on[1] = contact_groups[g][2];
      }

      power_off();
    }

    ~NeutralRelay(){
        delete[] pairs_on; 
        delete[] pairs_off; 
    }
  
    int state(){ return inner_state; }
  
    void power_on(){ inner_state = 1; }
  
    void power_off(){ inner_state = 0; }
  
    int** connected_contacts(){ return inner_state ? pairs_on : pairs_off; }
  
    int contact_connected_to(int contact){
      int** pairs = connected_contacts();
      for(int g = 0; g < group_count; g++){
        if(contact == pairs[g][0]){
          return pairs[g][1];
        }
        if(contact == pairs[g][1]){
          return pairs[g][0];
        }
      }
      return 0;
    }
  
    bool are_connected(int contact1, int contact2){
      return contact_connected_to(contact1) == contact2;
    }
    
};

/////////////////// Example of using (relay HH54P)

// There is no standard method to print 2-dimensional array...
void print_connected_contacts(NeutralRelay& relay, int group_count){ 
  int** contacts = relay.connected_contacts();
  cout << "Connected contacts: [";
  for(int g=0; g<group_count; g++){
    cout << "[";
    for(int c=0; c<2; c++){
       cout << contacts[g][c];
       if(c==0){ cout << ','; }
    }
    cout << "]";
    if(g<group_count-1){ cout << ','; }
  }
  cout << "]" << endl;
}

void test_contact(NeutralRelay& relay, int contact){
  cout << contact << " is connected to " << relay.contact_connected_to(contact) << endl;
}

void test_contact_pair(NeutralRelay& relay, int contact1, int contact2){
  cout << contact1 << " and " << contact2 << " are connected: " <<  relay.are_connected(contact1, contact2) << endl;
}

void test(NeutralRelay& relay){
  cout << "State: " << relay.state() << endl;
  print_connected_contacts(relay, 4);
  test_contact(relay, 1);
  test_contact(relay, 9);
  test_contact(relay, 5);
  test_contact_pair(relay, 1,9);
  test_contact_pair(relay, 9,1);
  test_contact_pair(relay, 9,5);
}
 
int main() {
  int contact_groups[4][3] = { {1, 9, 5}, {2, 10, 6}, {3, 11, 7}, {4, 12, 8} };
  NeutralRelay relay = NeutralRelay(contact_groups);
  test(relay);
  relay.power_on();
  test(relay);
}
