class NeutralRelay
  
  def initialize(contact_groups)
    @contacts = {
      false => contact_groups.map { |group| group.slice(0..1) },
      true => contact_groups.map.map { |group| group.slice(1..2) }
    }
    power_off!
  end
  
  def state
    @state ? 'ON' : 'OFF'
  end
  
  def power_on!
    @state = true
  end
  
  def power_off!
    @state = false
  end
  
  def connected_contacts
    @contacts[@state]
  end
  
  def contact_connected_to(contact)
    pair = connected_contacts.detect { |pair| pair.include?(contact)  }
    pair.detect { |c| c != contact } if pair
  end
  
  def connected?(contact1, contact2)
    contact1 == contact_connected_to(contact2)
  end
  
end

################### Example of using (relay HH54P)

contact_groups = [
  [1, 9, 5],
  [2, 10, 6], 
  [3, 11, 7], 
  [4, 12, 8]
]
relay = NeutralRelay.new(contact_groups)

test_contact = lambda do |contact|
  p "Contact #{contact} is connected to #{relay.contact_connected_to(contact)}"
end

test_contact_pair = lambda do |contact1, contact2|
  p "#{contact1} and #{contact2} are connected: #{relay.connected?(contact1, contact2)}"
end

test = lambda do
  p "State: #{relay.state}"
  p "Connected Contacts: #{relay.connected_contacts}"
  test_contact.call(1);
  test_contact.call(9);
  test_contact.call(5);
  test_contact_pair.call(1,9);
  test_contact_pair.call(9,1);
  test_contact_pair.call(9,5);
end

test.call
relay.power_on!
test.call
