require File.expand_path(File.join(File.dirname(__FILE__),'neutral_relay.rb'))

describe NeutralRelay do
  contact_groups = [ 
    [13, 11, 12],
    [23, 21, 22],
    [33, 31, 32], 
    [43, 41, 42],
    [53, 51, 52], 
    [63, 61, 62],
    [73, 71, 72],
    [83, 81, 82]
  ]       
  let(:relay) { described_class.new(contact_groups) }
  
  describe "#initialize" do
    it "should set state to 'OFF'" do
      relay.state.should == 'OFF'
    end
  end
  
  describe "#state getter" do
    it "should return 'OFF', when inner @state is false" do
      relay.instance_variable_get(:@state).should == false
      relay.state.should == 'OFF'   
    end
    
    it "should return 'ON', when inner @state is true" do
      relay.instance_variable_set(:@state, true)
      relay.instance_variable_get(:@state).should == true
      relay.state.should == 'ON'
    end
  end
  
  context 'swithing power' do
  
    describe "#power_on! should set state to 'ON'" do
      it "when state was 'OFF'" do
        relay.state.should == 'OFF'
        expect { relay.power_on! }.to change(relay, :state).to('ON')
      end
    
      it "when state was 'ON' (should not change the state)" do
        relay.instance_variable_set(:@state, true)
        relay.state.should == 'ON'
        expect { relay.power_on! }.to_not change(relay, :state)
      end
    end
  
    describe "#power_off! should set state to 'OFF'" do
      it "when state was 'ON'" do
        relay.instance_variable_set(:@state, true)
        relay.state.should == 'ON'
        expect { relay.power_off! }.to change(relay, :state).to('OFF')
      end
    
      it "when state was 'OFF' (should not change the state)" do
        relay.state.should == 'OFF'
        expect { relay.power_off! }.to_not change(relay, :state)
      end
    end
    
  end

  describe "#contact_connected_to should return number of a contact, which is connected to the passed one" do
  
    context 'wnen relay is not energized' do
      before(:each) { relay.power_off! }
      
      contact_groups.each do |group|
        it "should return #{group[0]} for #{group[1]}" do
          relay.contact_connected_to(group[1]).should == group[0]
        end
      end
      
      contact_groups.each do |group|
        it "should return #{group[1]} for #{group[0]}" do
          relay.contact_connected_to(group[1]).should == group[0]
        end
      end
      
      contact_groups.each do |group|
        it "should return nil for #{group[2]}" do
          relay.contact_connected_to(group[2]).should be_nil
        end
      end
      
      it "should return nil for any wrong contact number" do
        relay.contact_connected_to(5).should be_nil
      end
    end
    
    context 'wnen relay is energized' do
      before(:each) { relay.power_on! }
      
      contact_groups.each do |group|
        it "should return #{group[2]} for #{group[1]}" do
          relay.contact_connected_to(group[1]).should == group[2]
        end
      end
      
      contact_groups.each do |group|
        it "should return #{group[1]} for #{group[2]}" do
          relay.contact_connected_to(group[2]).should == group[1]
        end
      end
      
      contact_groups.each do |group|
        it "should return nil for #{group[0]}" do
          relay.contact_connected_to(group[0]).should be_nil
        end
      end
      
      it "should return nil for any wrong contact number" do
        relay.contact_connected_to(5).should be_nil
      end
    end
  
  end
  
  describe "#connected_contacts should return array of connected contact pairs" do
    context 'wnen relay is energized' do
      before(:each) { relay.power_on! }
      
      it "" do
        relay.connected_contacts.should == contact_groups.map {|group| [group[1], group[2]] }
      end
    end
    
    context 'wnen relay is not energized' do
      before(:each) { relay.power_off! }
      
      it "" do
        relay.connected_contacts.should == contact_groups.map {|group| [group[0], group[1]] }
      end
    end
  end

  describe "#connected?" do
    before(:each) do
      relay.power_on!
    end
    
    it "should return true, if passed contacts are connected" do
      relay.connected?(11, 12).should be_true
    end
    
    it "should return false, if passed contacts are not connected" do
      relay.connected?(11, 13).should be_false
    end
  end
end
